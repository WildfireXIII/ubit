import serial # (pyserial)
import time

print("Connecting...")
with serial.Serial('/dev/ttyACM0', 115200) as ser:
    print("Connected!")
    print("Sending...")
    ser.write(b'hello there\n')
    line = ser.readline()
    print(line)
    time.sleep(1)
