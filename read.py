import serial

with serial.Serial('/dev/ttyACM0', 115200) as ser:
    print("connected")
    ser.write(b"get reading\n")
    ser.flush()
    line = str(ser.readline())
    reading = line[:line.find(' ')]
    print(reading)
