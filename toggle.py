import serial # (pyserial)

with serial.Serial('/dev/ttyACM0', 115200) as ser:
    print("Connected")
    ser.write(b"toggle light\n")
    ser.flush()
